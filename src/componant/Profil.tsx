import { ReactNode } from 'react'



type profilProps = {
    children?: ReactNode
    name:string
}



function profil(props:profilProps){
    return (
        <div className='profil'>
            <p>{props.name}</p>
            {props.children}
        </div>
    )
}

export default profil