import Search from './search'
import Profil from './Profil'
type headerProps = {
    st:string
    setst:Function
}



function header(props:headerProps){
    
    return(
        <div className="header">
            <p className="logo"></p>
            <Search st={props.st} setst={props.setst}/>
            <Profil name='Anthony'>
                <select name="" id="">
                    <option value="">Francais</option>
                    <option value="">English</option>
                </select>
            </Profil>    
        </div>
    )
}

export default header