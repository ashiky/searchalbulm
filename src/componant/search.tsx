
type searchProps = {
    st:string
    setst:Function
}

function search(props:searchProps){
    
   
    return (
        <div>
            <div className="search">
            <input 
                type="search"
                placeholder="Rechercher..."
                onChange={(event) => props.setst(event.target.value)} />
            </div>
        </div>
    )
}

export default search