
type albumProps = {
    title:string
    image:string
    style?:string
    langue?:string
}


function album(props:albumProps){
    return (
        <div className="album" style={{backgroundImage: `url(${props.image})` }}>
            <h2>{props.title}</h2>
        </div>
    )
}
    export default album