
import './App.css'
import Content from './componant/Content'
import Header from './componant/header'
import { useState, useEffect } from "react";
function App() {
    const [searchTerm , setSearchTerm] = useState("");
  return (
    <div className="App">
      <Header st={searchTerm} setst={setSearchTerm}/>
       <Content st={searchTerm} />
    </div>
  )
}
export default App
